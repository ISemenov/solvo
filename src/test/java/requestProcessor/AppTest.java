package requestProcessor;

import org.junit.jupiter.api.Test;
import requestProcessor.builders.RndValRequestBuilder;
import requestProcessor.request.RequestType;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

class AppTest {

    @Test
    void Main(){

        int requestTypeACount = 50;
        int requestTypeBCount = 50;

        EntryPoint entryPoint = new EntryPoint();

        RequestGenerator generatorA = new RequestGenerator(new RndValRequestBuilder(RequestType.A), entryPoint, requestTypeACount);
        RequestGenerator generatorB = new RequestGenerator(new RndValRequestBuilder(RequestType.B), entryPoint, requestTypeBCount);

        QueueProcessor queueProcessorA = new QueueProcessor(entryPoint.getRequestQueueA());
        QueueProcessor queueProcessorB = new QueueProcessor(entryPoint.getRequestQueueB());

        ExecutorService service = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

        service.execute(generatorA);
        service.execute(generatorB);
        service.execute(queueProcessorA);
        service.execute(queueProcessorB);

        service.shutdown();
        try {
            service.awaitTermination(2, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}