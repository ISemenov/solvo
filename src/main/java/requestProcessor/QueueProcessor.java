package requestProcessor;

import requestProcessor.request.Request;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class QueueProcessor implements Runnable {

    private RequestQueue queue;
    private ExecutorService service = Executors.newFixedThreadPool(5);

    public QueueProcessor(RequestQueue queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        while (true) {
            service.execute(() -> {
                Request request = queue.get();
                Random random = new Random();
                int sleep = random.nextInt(100);
                if (request != null) {
                    try {
                        Thread.sleep(sleep);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Queue type: " + queue.getType() +
                            ", Request type: " + request.getType() +
                            ", Request value: " + request.getValue() +
                            ", slept : " + sleep);
                }
            });
        }
    }
}
