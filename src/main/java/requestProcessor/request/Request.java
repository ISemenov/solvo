package requestProcessor.request;

public class Request{

    private int value;
    private final RequestType type;

    public Request(RequestType type, int value) {
        this.type = type;
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public RequestType getType() {
        return type;
    }
}
