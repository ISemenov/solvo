package requestProcessor;

import requestProcessor.builders.RequestBuilder;

public class RequestGenerator implements Runnable {

    private EntryPoint entry;
    private int requestsCount;
    private RequestBuilder builder;

    public RequestGenerator(RequestBuilder builder, EntryPoint entry, int count) {
        this.entry = entry;
        this.requestsCount = count;
        this.builder = builder;
    }

    @Override
    public void run() {
        int count = 0;
        while (count < requestsCount) {
            count++;
            entry.add(builder.build());
        }
    }

}
