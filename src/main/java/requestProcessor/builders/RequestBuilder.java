package requestProcessor.builders;

import requestProcessor.request.Request;

public interface RequestBuilder<T extends Request> {
    T build();
}
