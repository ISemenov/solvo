package requestProcessor.builders;

import requestProcessor.request.Request;
import requestProcessor.request.RequestType;

import java.util.Random;

public class RndValRequestBuilder implements RequestBuilder {

    private static final int MAX = 50;
    private static final int MIN = 0;

    private RequestType type;

    public RndValRequestBuilder(RequestType type) {
        this.type = type;
    }

    @Override
    public Request build() {
        return new Request(type, new Random().nextInt((MAX - MIN) + 1) + MIN);
    }
}
