package requestProcessor;

import requestProcessor.request.Request;
import requestProcessor.request.RequestType;

public class EntryPoint {

    private RequestQueue requestQueueA;
    private RequestQueue requestQueueB;

    public EntryPoint() {
        this.requestQueueA = new RequestQueue(RequestType.A);
        this.requestQueueB = new RequestQueue(RequestType.B);
    }

    public synchronized void add(Request request) {
        switch (request.getType()) {
            case A:
                requestQueueA.add(request);
                break;
            case B:
                requestQueueB.add(request);
                break;
            default:
                throw new RuntimeException("Unknown request type");
        }

    }

    public RequestQueue getRequestQueueA() {
        return requestQueueA;
    }

    public RequestQueue getRequestQueueB() {
        return requestQueueB;
    }
}
