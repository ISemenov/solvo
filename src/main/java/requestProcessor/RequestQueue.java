package requestProcessor;

import requestProcessor.request.Request;
import requestProcessor.request.RequestType;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class RequestQueue {

    private List<Request> queue;
    private RequestType type;
    private static final int MAX_REQUESTS_IN_STORE = 5;
    private static final int MIN_REQUESTS_IN_STORE = 0;

    private int requestCounter = 0;

    public RequestQueue(RequestType type) {
        queue = new ArrayList<>();
        this.type = type;
    }

    private boolean hasSameRequestInQueue(List<Request> queue, Request request) {
        Optional<Request> first = queue.stream().filter(x -> x.getValue() == request.getValue()).findFirst();
        return first.isPresent();
    }

    public synchronized boolean add(Request request) {
        try {
            if (requestCounter < MAX_REQUESTS_IN_STORE && !hasSameRequestInQueue(queue, request)) {
                notifyAll();
                queue.add(request);
                requestCounter++;
                System.out.println("Queue of type: " + type.name() + ", size: " + requestCounter);
            } else {
                wait();
                return false;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return true;
    }

    public synchronized Request get() {
        try {
            if (requestCounter > MIN_REQUESTS_IN_STORE) {
                notifyAll();
                Request request = queue.get(0);
                queue.remove(request);
                requestCounter--;
                return request;
            }
            wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public RequestType getType() {
        return type;
    }

}
